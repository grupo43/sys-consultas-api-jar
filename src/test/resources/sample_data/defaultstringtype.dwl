[
  {
    "id_periodo": "2020-1",
    "id_asignatura": 123,
    "asignatura": "Asignatura cursada",
    "condicion": "aprobado",
    "notas": [
      {
        "id_nota": 1,
        "calificacion": "7",
        "tipo_evaluacion": "parcial"
      },
      {
        "id_nota": 2,
        "calificacion": "7",
        "tipo_evaluacion": "parcial"
      },
      {
        "id_nota": 3,
        "calificacion": "5",
        "tipo_evaluacion": "final"
      }
    ]
  },
  {
    "id_periodo": "2020-1",
    "id_asignatura": 124,
    "asignatura": "Asignatura cursada",
    "condicion": "aprobado",
    "notas": [
      {
        "id_nota": 4,
        "calificacion": "7",
        "tipo_evaluacion": "parcial"
      },
      {
        "id_nota": 5,
        "calificacion": "7",
        "tipo_evaluacion": "parcial"
      },
      {
        "id_nota": 6,
        "calificacion": "5",
        "tipo_evaluacion": "final"
      }
    ]
  }
]